using System;
using UnityEngine;
using UnityEngine.Events;

public class UnityEventsDemoComponent : MonoBehaviour
{

    [Serializable]
    public class ExampleEvent : UnityEvent<bool>
    {

    }

#pragma warning disable CS0649
    [SerializeField]
    private ExampleEvent EventFired;
#pragma warning restore CS0649

    private void Update()
    {

        EventFired?.Invoke(true);

    }

}
