# UnityEvents Bug Demo

> Regression from 2019.2.4f1 to 2019.2.5f1

Custom UnityEvents with generics no longer serialize correctly in the Unity editor.

<https://docs.unity3d.com/Manual/UnityEvents.html>

```csharp
using System;
using UnityEngine;
using UnityEngine.Events;

public class UnityEventsDemoComponent : MonoBehaviour
{

    [Serializable]
    public class ExampleEvent : UnityEvent<bool>
    {

    }

    [SerializeField]
    private ExampleEvent EventFired;

}
```

## 2019.2.4f1 (before)

<img src="./Screenshots/unityevent-before.png" width="400">

## 2019.2.5f1 (after)

<img src="./Screenshots/unityevent-after.png" width="400">
